package week2.practicum;
import LIB.TextIO;

public class Exercises {

public static void main(String[] args) {
	String name;
	int shoes;
	int numberOne;
	int numberTwo;
	int calculator;
	int people;
	int groupSize;
	int nameLenght;
	String charChanger;
	
	//NAME AND SHOE SIZE + NAME LENGHT CALCULATION
	System.out.println("What is your name?");
	name = TextIO.getlnString();
	nameLenght = name.length();
	System.out.println("Hello " + name + ", it is nice to meet you. Did you know your name has "+nameLenght+" letters?");
	System.out.println("Can you tell me your shoe size? Pretty please??!?!");
	shoes = TextIO.getInt();
	System.out.println("Wow " + name + (", so your shoe size is ") + shoes + (". Awesome!"));
	System.out.println("--------------------");
	String newName = "John";
	int nameLength = name.length();
	System.out.println(nameLength);
	System.out.println("four".length());
	
	// INSERT TWO NUMBERS
//	System.out.println("Now, lets play a game. Please enter two numbers.");
//	numberOne = TextIO.getInt();
//	numberTwo = TextIO.getInt();
//	calculator = numberOne + numberTwo;
//	System.out.println("You entered: " + numberOne + " and " + numberTwo +" and the sum of those is " + calculator);
//	System.out.println("--------------------");
	
//	// MODULE CALCULATION
//	System.out.println("Now, lets do some module calculation, enter the number of people and the size of a group");
//	people = TextIO.getInt();
//	groupSize = TextIO.getInt();
//	System.out.println("So, you have " + people + " people in total and the group size is " + groupSize +". If you group up all the people, " + people%groupSize + " people will be left out. Boo-Hoo!");

//	// Word Replacer
	System.out.println("Lets do some magic. I will change all the letters 'a' to something else. Please enter a sentence now:");
	charChanger = TextIO.getlnString();
	
	System.out.println(charChanger.replace('a', '!'));
	
}
	
}
