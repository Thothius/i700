package week2.practicum;

import java.util.Random;
import LIB.TextIO;

public class NumberWizard {

	public static void main(String[] args) {

		Random randomNumber = new Random();
		int min = 1;
		int max = 1000;
		int n;
		int guess;
		boolean gameOver = false;
		n = randomNumber.nextInt(10);
		System.out.println("I will pick a random number from 1 to 10 and you will try to guess what it is.");
		
		while (gameOver == false) {
			guess = TextIO.getInt();
			if (guess == 0) {
				guess += 1;
			}
			if (guess == n) {
				System.out.printf("You win, the number was %d", n);
				gameOver = true;
			} else if (guess > n) {
				System.out.printf("Wrong, try again. The number is smaller than %d", guess);
			} else if (guess < n) {
				System.out.printf("Wrong, try again. The number is bigger than %d", guess);
			}
		}

	}
}
