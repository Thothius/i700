package week3.practicum;

import java.util.Scanner;

import LIB.*;

public class Password {

	static String password;
	static String input;
	static int n = 3;
	static String inputChecker;
	static String help = "help";
	static String psw = "psw";
	static String exit = "exit";
	static String root = "root";
	static boolean loggingIn = false;

	public static void main(String[] args) {
		System.out.println("<Accessing system configuration settings>");
		System.out.println("<Type psw to set password or exit to quit the program>");
		inputChecker = TextIO.getlnString();


		// PASSWORD SECTION
		if (inputChecker.equals(psw)) {

			System.out.println("Set up your super-secret password. Must not contain any ");
			password = TextIO.getlnString();
			
	       
		 
		    } else if (inputChecker.equals(exit)) {
				System.out.println("<Logging out>");
				System.exit(0); 
		    } else{
			
			System.out.println("Password saved. System reboot required");
			System.out.println("<Logged out>");
			System.out.println("<Restarting server>");
			System.out.println("<Logging in>");
			loggingIn = true;
		    }

			// EXIT PROGRAM
		 if (inputChecker.equals(exit)) {
			System.out.println("<Logging out>");
			System.exit(0);

			// BE GOD SECTION
		} else if (inputChecker.equals(root)) {
			System.out.println("YOU ARE GOD AND DONT NEED ANY PASSWORDS!");
			inputChecker = TextIO.getlnString();
		} else {
			// COMMAND IS UNKNOWN
			System.out.println("Unknown command. Use help to see the list of available commands.");
			inputChecker = TextIO.getlnString();
		}

		// PASSWORD CHECKER
		while (n >= 1 && loggingIn == true) {
			System.out.println("Insert password: ");
			input = TextIO.getlnString();

			if (password.equals(input)) {
				System.out.println("<Logged in>");
				break;
				// ACCOUNT BLOCKER
			} else if (n == 0) {
				System.out.println("Yout user has been blocked. Please contact yout system administrator. Good day.");
				n -= 1;
			} else {
				System.out.println("Wrong password. Try again. You have " + n + " tries left.");
			}
		}

	}
}
