package week3.practicum;

import LIB.TextIO;

public class CumLaude {

	static double gradeAverage;
	static int thesis;

	public static void main(String[] args) {

		System.out.println("Insert your average grade: ");
		gradeAverage = TextIO.getDouble();

		System.out.println("Insert your thesis grade: ");
		thesis = TextIO.getInt();

		// Negative Value Checker
		if (gradeAverage < 0) {
			System.out.println("Your average grade can't be negative. Enter it again.");
			gradeAverage = TextIO.getDouble();
		}
		if (thesis < 0) {
			System.out.println("Your thesis grade can't be negative. Enter it again.");
			thesis = TextIO.getInt();
		}

		if (gradeAverage >= 4.5 && thesis == 5) {
			System.out.println("Congratz! You are eligable for Cum Laude.");
		} else {
			System.out.println("Sorry brawh, no Cum Laude for you.");
		}
	}

}
