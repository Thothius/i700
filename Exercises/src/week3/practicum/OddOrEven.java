package week3.practicum;

import LIB.TextIO;

public class OddOrEven {

	static int number;

	public static void main(String[] args) {
		System.out.println("Insert a number:");
		number = TextIO.getInt();

		if (number % 2 == 0) {
			System.out.println("Your number is even.");
		} else
			System.out.println("Your number is odd.");
	}

}
