package week3.practicum;

import LIB.TextIO;

public class CoupleAges {

	static int human1;
	static int human2;
	static int difference;

	public static void main(String[] args) {

		System.out.println("Insert the age of the first human: ");
		human1 = TextIO.getInt();
		if (human1 < 0) {
			System.out.println("You can't have a negative age for your first human. Insert an age again.");
			human1 = TextIO.getInt();
		}

		System.out.println("Insert the age of the second human: ");
		human2 = TextIO.getInt();
		if (human2 < 0) {
			System.out.println("You can't have a negative age for your second human. Insert an age again.");
			human2 = TextIO.getInt();
		}

		difference = Math.abs(human1 - human2);

		// If the difference of ages is 5..10 years, the program tells "quite
		// okay"
		// If the difference of ages is 11..15 years, the program tells "not
		// that okay"
		// If the difference of ages is more than 15 years, the program tells
		// "not okay"
		// If the difference of ages is less than 5 years, the program tells
		// "very nice"

		if (difference < 5) {
			System.out.println("Very nice!");
		} else if (difference > 15) {
			System.out.println("Not okay!");
		} else if (difference >= 11 && difference <= 15) {
			System.out.println("Not that okay!");
		} else {
			System.out.println("Quite okay!");
		}

	}

}
